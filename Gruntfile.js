module.exports = function (grunt) {
    var pkg = grunt.file.readJSON("package.json");

    grunt.initConfig({
        pkg: pkg,

        mocha: {
            test: {
                src: ['test/spec-runner.html']
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            lib: {
                src: [
                    'lib/**/*.js'
                ]
            },
            test: {
                src: [
                    'test/**/*.js'
                ]
            }
        },

        uglify: {
            build: {
                options: {
                    banner: [
                        "/**",
                        " * WizardJS - <%= pkg.version %>",
                        " * Author   - <%= pkg.author %>",
                        " * License  - <%= pkg.license %>",
                        " */",
                        ""
                    ].join("\n")
                },
                src: ['lib/**/*.js'],
                dest: 'dist/wizard.min.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-mocha');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('test', ['jshint:lib', 'jshint:test', 'mocha:test']);
    grunt.registerTask('build', ['test', 'uglify']);
};