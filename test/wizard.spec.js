/* global: describe, it */
(function (w, assert) {
    "use strict";

    describe("Wizard", function () {

        it('can be extended', function () {
            assert.equal(typeof w.extend, 'function');

            var protoOne = w.extend();
            var protoTwo = w.extend();

            assert.equal(Object.getPrototypeOf(protoOne), w);
            assert.equal(Object.getPrototypeOf(protoTwo), w);
        });

        it('can create prototype chains', function () {
            var protoOne = w.extend();
            var protoTwo = protoOne.extend();

            assert.equal(Object.getPrototypeOf(protoTwo), protoOne);
        });

        it('can resolve init without providing an init for derived object', function () {
            var value = 1;
            var protoOne = w.extend()
                .init(function (value) {
                    this.public({
                        a: value
                    });
                });
            var protoTwo = protoOne.extend();
            var instance = protoTwo.create(value);

            assert.equal(Object.getPrototypeOf(protoTwo), protoOne);
            assert.equal(instance.a, value);
        });

        it('can determine inheritance by reflection', function () {
            var protoOne = w.extend();
            var protoTwo = protoOne.extend();
            var instance = protoTwo.create();

            assert.ok(instance.is(protoTwo));
            assert.ok(instance.is(protoOne));
        });

        it('can resolve super\' init with .inherit()', function () {
            var value = 1;
            var protoOne = w.extend()
                .init(function (value) {
                    this.public({
                        a: value
                    });
                });
            var protoTwo = protoOne.extend()
                .init(function () {
                    protoOne.inherit(this, arguments);
                });
            var instance = protoTwo.create(value);

            assert.equal(instance.a, value);
        });

        it('can create public properties with a hash', function () {
            var protoOne = w.extend()
                .public({
                    a: 1
                });

            assert.deepEqual(Object.getOwnPropertyDescriptor(protoOne, 'a'), {
                enumerable: true,
                writable: true,
                configurable: true,
                value: 1
            });
        });

        it('can create public properties with key, value', function () {
            var protoOne = w.extend()
                .public('a', 1);

            assert.deepEqual(Object.getOwnPropertyDescriptor(protoOne, 'a'), {
                enumerable: true,
                writable: true,
                configurable: true,
                value: 1
            });
        });

        it('can create private properties with a hash', function () {
            var protoOne = w.extend()
                .private({
                    a: 1
                });

            assert.deepEqual(Object.getOwnPropertyDescriptor(protoOne, 'a'), {
                enumerable: false,
                writable: true,
                configurable: false,
                value: 1
            });
        });

        it('can create private properties with key, value', function () {
            var protoOne = w.extend()
                .private('a', 1);

            assert.deepEqual(Object.getOwnPropertyDescriptor(protoOne, 'a'), {
                enumerable: false,
                writable: true,
                configurable: false,
                value: 1
            });
        });

        it('can create static properties with a hash', function () {
            var protoOne = w.extend()
                .constant({
                    a: 1
                });

            assert.deepEqual(Object.getOwnPropertyDescriptor(protoOne, 'a'), {
                enumerable: true,
                writable: false,
                configurable: false,
                value: 1
            });
        });

        it('can create static properties with key, value', function () {
            var protoOne = w.extend()
                .constant('a', 1);

            assert.deepEqual(Object.getOwnPropertyDescriptor(protoOne, 'a'), {
                enumerable: true,
                writable: false,
                configurable: false,
                value: 1
            });
        });

        it('can create instances of derived classes', function () {
            var value = 1;
            var proto = w.extend()
                .init(function (param) {
                    this.public({
                        a: param
                    });
                });
            var instance = proto.create(value);

            assert.equal(Object.getPrototypeOf(instance), proto);
            assert.ok(instance instanceof proto.constructor);
            assert.ok(instance.is(proto));
            assert.ok(instance.hasOwnProperty('a'));
            assert.equal(instance.a, value);
        });

        it('can mix objects', function () {
            var proto = w.extend();
            var mixin1 = {
                a: 1
            };
            var mixin2 = {
                b: 2
            };

            proto.mix(mixin1, mixin2);

            assert.equal(proto.a, 1);
            assert.ok(proto.mixes(mixin1, mixin2));
        });

    });
}(wizard, chai.assert));