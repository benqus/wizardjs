(function (root, definition) {
    // UMD module definition
    // see more {@link https://github.com/umdjs/umd}

    if (typeof define === 'function' && define.amd) {
        // AMD with define
        define('wizard', [], definition);

    } else if (typeof module === 'object' && module.exports) {
        // CommonJS with module
        module.exports = definition();

    } else {
        // browser with window (root)
        root.wizard = definition();

    }
}((function () {
    // all functions have the global object as their context (this)
    // by default - except in strict mode. The way you invoke the
    // function may exchange the context. In this case the function
    // is declared in the global scope and it is invoked on its own
    // so this will refer to the global object (window).
    return this;
}()), function () {
    // strict mode
    'use strict';

    var slice = Array.prototype.slice;

    /**
     * Determines whether the context (this) inherits
     * from the provided prototype object.
     * @memberof  wizard
     * @param    {object} proto - the prototype object
     * @returns  {boolean}
     */
    var is = function (proto) {
        // In JS it does not matter how you built your object,
        // for "reflection" it only matters that your object's
        // constructor is a certain function.
        return (this instanceof proto.constructor);
    };

    /**
     * Mixes the context with the provided mixins.
     * @memberof   wizard
     * @arguments
     * @returns   {object}
     */
    var mix = function () {
        var args = slice.call(arguments, 0);
        var mixin;
        var i;

        while (args.length > 0) {
            mixin = args.shift();
            for (i in mixin) {
                if (mixin.hasOwnProperty(i)) {
                    this[i] = mixin[i];
                }
            }
        }

        return this;
    };

    /**
     * Defines a 'constructor' (initializer) a
     * decorator for after object creation.
     * @memberof  wizard
     * @param    {function} decorator
     * @returns  {object}
     */
    var init = function (decorator) {
        decorator.prototype = this;
        this.constructor = decorator;
        return this;
    };

    /**
     * Determines whether the context has been mixed with the provided mixins.
     * @memberof   wizard
     * @arguments
     * @returns   {boolean}
     */
    var mixes = function () {
        var args = slice.call(arguments, 0);
        var mixin;
        var i;

        while (args.length > 0) {
            mixin = args.shift();
            for (i in mixin) {
                if (mixin.hasOwnProperty(i) && !this.hasOwnProperty(i)) {
                    return false;
                }
            }
        }

        return true;
    };

    /**
     * Creates (builds) an object on top of the context (this).
     * Preferably, this method should be invoked on a prototype object.
     * @memberof   wizard
     * @arguments
     * @returns   {object}
     */
    var create = function () {
        var instance = Object.create(this);
        this.inherit(instance, arguments);
        return instance;
    };

    /**
     * Creates a new prototype object.
     * @memberof  wizard
     * @returns  {object}
     */
    var extend = function () {
        var self    = this;
        var derived = Object.create(self);

        /**
         * helper method to help inherit init (constructor) behaviour.
         * @param   {object}          context - object to inherit onto
         * @param   {Array|arguments} args    - arguments
         * @returns {*}
         */
        derived.inherit = function (context, args) {
            return this.constructor.apply(context, args);
        };

        /**
         * Initial init (constructor) method to help resolve inheritance.
         * @returns {*}
         */
        derived.constructor = function () {
            return self.constructor.apply(this, arguments);
        };

        derived.constructor.prototype = self;

        return derived;
    };

    /**
     * Creates a closure for decorators that behave similarly.
     * @memberof  wizard
     * @param     decorator
     * @returns  {Function}
     */
    var decoratorClosureFactory = function (decorator) {

        /**
         * Decorates the context with
         * @memberof  wizard
         * @param    {object|string} arg
         * @param    {*}             value
         * @returns  {object}
         */
        return function (arg, value) {
            var i;

            if (arg && typeof arg === 'object') {
                for (i in arg) {
                    if (arg.hasOwnProperty(i)) {
                        decorator.call(this, i, arg[i]);
                    }
                }
            } else if (typeof arg === 'string') {
                decorator.call(this, arg, value);
            }

            return this;
        };
    };

    /**
     * Decorates the context (this) with enumerable, mutable attributes.
     * @namespace  wizard
     * @param     {string} name
     * @param     {*}      value
     */
    var createPublicAttribute = function (name, value) {
        this[name] = value;
    };

    /**
     * Decorates the context (this) with non-enumerable, mutable attributes.
     * @namespace  wizard
     * @param     {string} name
     * @param     {*}      value
     */
    var createPrivateAttribute = function (name, value) {
        Object.defineProperty(this, name, {
            writable: true,
            value: value
        });
    };

    /**
     * Decorates the context (this) with enumerable, immutable attributes.
     * @namespace  wizard
     * @param     {string} name
     * @param     {*}      value
     */
    var createConstantAttribute = function (name, value) {
        Object.defineProperty(this, name, {
            enumerable: true,
            value: value
        });
    };

    /**
     * Wizard namespace & API
     * @namespace wizard
     */
    return {
        is:       is,
        mix:      mix,
        init:     init,
        mixes:    mixes,
        create:   create,
        public:   decoratorClosureFactory(createPublicAttribute),
        extend:   extend,
        private:  decoratorClosureFactory(createPrivateAttribute),
        constant: decoratorClosureFactory(createConstantAttribute)
    };
}));