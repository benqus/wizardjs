Wizard
===

[ ![Codeship Status for benqus/wizardjs](https://www.codeship.io/projects/9a5768d0-f4e6-0131-d3ff-7a826438f703/status)](https://www.codeship.io/projects/28217)
[![Built with Grunt](https://cdn.gruntjs.com/builtwith.png)](http://gruntjs.com/)

Prototype-chain builder tool for JavaScript (ES5).

Wizard's source is meant to be researched in order to understand how
JavaScript works. This also required the source to be relatively small.

> **Note:** Because Wizard is meant to be a tool to help you learn the
prototype-oriented behaviour and inheritance it is recommended to
use it the browser where an visual/interactive console is available.

Wizard was inspired by and it is a tiny version of [Troop](https://github.com/production-minds/troop "Troop").
Large but great stuff!!!

## Namespace
---

    // AMD or CommonJS (NodeJS)
    var wizard = require('wizard');

    // browser
    (function (wizard) {
        // do something here...
    }(window.wizard));

## Instantiation
---

Creating objects that inherit from (are build upon) wizard object

    var instance = wizard.create();

Or `[subClass].create([arguments]);`. For *inheritance* see next paragraph.

## Inheritance
---

Creating classes that inherit from (are build upon) wizard object or each other

    var protoOne = wizard.extend();

Inheriting from `protoOne`

    // notice  >>  protoOne
    var protoTwo = protoOne.extend();

'Type' checking

    var instance = protoTwo.create();
    instance.is(protoTwo); // true
    instance.is(protoOne); // true

## Constructor

Wizard has a way of specifying functions that *decorate* a newly created
object to a certain schema via the `.init([initializer])`.

    protoOne
        .init(function (arg) {
            this.a = arg;
        }):

Instantiation

    var instance = protoOne.create("some random string for value");
    instance.a; // "some random string for value"

Inheritance

> **Note:** Unless you want to extend the initializer method with
custom behaviour you are **not** required to specify
an initializer method for the derived object.

    protoTwo
        .init(function () {
            protoOne.inherit(this, arguments);
        });

## Adding features
---

There are 3 *types* of properties:

- `public` (enumerable, mutable),
- `private` (non-enumerable, mutable),
- `constant` (enumerable, immutable).

### creating properties

On the prototype

    var protoThree = wizard.extend()

        // enumerable, immutable prototype attributes
        .constant({
            TYPE: 'protoOne'
        })

        // non-enumerable, mutable prototype attributes
        .private({
            _onClickListener: function (e) {
                // ...
            }
        })

        // public prototype interface
        .public({
            getName: function () {
                return this._name;
            }
        });

On an instance

    protoThree
        .init(function () {

            // scope of the instance
            this
                // enumerable, immutable instance attributes
                .constant({
                    ID: 1
                })

                // non-enumerable, mutable instance attributes
                .private({
                    _name: 'me'
                })

                // public instance interface
                .public({
                    greeting: 'Hello!'
                });
        });

## Mixins

Mixing objects happens via the `.mix([obj1 [, obj2 [, ... [, objN]]]])` method.

    protoThree
        .mix({ a: 1 }, { b : 2 })

    protoThree.mixes({ a: 1 }); // true